import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { About1Page } from '../about1/about1';
import { About2Page } from '../about2/about2';
import { About3Page } from '../about3/about3';
import { About4Page } from '../about4/about4';
import { About5Page } from '../about5/about5';
import { About6Page } from '../about6/about6';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  home(){
    this.navCtrl.pop()
  }

  about1(){
    this.navCtrl.push(About1Page)
  }

  about2(){
    this.navCtrl.push(About2Page)
  }

  about3(){
    this.navCtrl.push(About3Page)
  }

  about4(){
    this.navCtrl.push(About4Page)
  }

  about5(){
    this.navCtrl.push(About5Page)
  }

  about6(){
    this.navCtrl.push(About6Page)
  }
}
