import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { About1Page } from '../pages/about1/about1';
import { About2Page } from '../pages/about2/about2';
import { About3Page } from '../pages/about3/about3';
import { About4Page } from '../pages/about4/about4';
import { About5Page } from '../pages/about5/about5';
import { About6Page } from '../pages/about6/about6';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    About1Page,
    About2Page,
    About3Page,
    About4Page,
    About5Page,
    About6Page
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    About1Page,
    About2Page,
    About3Page,
    About4Page,
    About5Page,
    About6Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
