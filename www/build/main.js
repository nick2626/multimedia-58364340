webpackJsonp([7],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return About1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the About1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var About1Page = /** @class */ (function () {
    function About1Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    About1Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad About1Page');
    };
    About1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about1',template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\pages\about1\about1.html"*/'<!--\n  Generated template for the About1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding >\n    <ion-grid>\n        <ion-row>\n          <ion-col class="center">\n            <div class="c">\n                  <img src="/assets/imgs/nicknew2.svg" class="c1" >\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <div class="center">\n          <div>\n              ปัสสาวะบ่อยผิดปกติ หากใครที่รู้สึกปวดท้องปัจสาวะบ่อยแม้ไม่ได้ดื่มน้ำมากก็ตาม ให้ระวังเอาไว้ว่าอาจจะเป็นสัญญาณเตือนของโรคเบาหวานก็เป็นได้\n          </div>\n      </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\work2019\multimedai\AppName\src\pages\about1\about1.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]) === "function" && _b || Object])
    ], About1Page);
    return About1Page;
    var _a, _b;
}());

//# sourceMappingURL=about1.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return About2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the About2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var About2Page = /** @class */ (function () {
    function About2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    About2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad About2Page');
    };
    About2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about2',template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\pages\about2\about2.html"*/'<!--\n  Generated template for the About1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n  \n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n      <ion-grid>\n          <ion-row>\n            <ion-col class="center1">\n              <div class="c">\n                    <img src="/assets/imgs/nicknew11.svg" class="c1" >\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <div class="center">\n            <div>\n                คอแห้ง กระหายน้ำบ่อย เช่นกัน หากใครที่รู้สึกว่าดื่มน้ำเท่าไหร่ก็ยังกระหายอยู่ดี ก็อาจจะเป็นสาเหตุมาจากโรคเบาหวาน\n            </div>\n        </div>\n  \n  </ion-content>\n  '/*ion-inline-end:"D:\work2019\multimedai\AppName\src\pages\about2\about2.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]) === "function" && _b || Object])
    ], About2Page);
    return About2Page;
    var _a, _b;
}());

//# sourceMappingURL=about2.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return About3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the About3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var About3Page = /** @class */ (function () {
    function About3Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    About3Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad About3Page');
    };
    About3Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about3',template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\pages\about3\about3.html"*/'<!--\n  Generated template for the About1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n  \n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n      <ion-grid>\n          <ion-row>\n            <ion-col class="center">\n              <div class="c">\n                    <img src="/assets/imgs/nicknick.svg" class="c1" >\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <div class="center">\n            <div>\n                ผิวหนังแห้งและคัน ก็อาจเป็นผลมาจากโรคเบาหวานได้ น้ำตาลในเลือดเยอะจะทำให้ผิวแห้ง\n            </div>\n        </div>\n  \n  </ion-content>\n  '/*ion-inline-end:"D:\work2019\multimedai\AppName\src\pages\about3\about3.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]) === "function" && _b || Object])
    ], About3Page);
    return About3Page;
    var _a, _b;
}());

//# sourceMappingURL=about3.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return About4Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the About4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var About4Page = /** @class */ (function () {
    function About4Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    About4Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad About4Page');
    };
    About4Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about4',template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\pages\about4\about4.html"*/'<!--\n  Generated template for the About1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n  \n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n      <ion-grid>\n          <ion-row>\n            <ion-col class="center">\n              <div class="c">\n                    <img src="/assets/imgs/nicknew13.svg" class="c1" >\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <div class="center">\n            <div>\n                น้ำหนักตัวลดโดยไม่ทราบสาเหตุ ทั้งๆ ที่ทานเยอะ แต่ทำไมน้ำหนักไม่ขึ้น แถมยังลดแบบไม่มีสาเหตุอีกด้วย ต้องรีบไปหมอหมอเช็คด่วน เพราะนี่เป็นสัญญาณเตือนที่อันตรายมาก\n            </div>\n        </div>\n  \n  </ion-content>\n  '/*ion-inline-end:"D:\work2019\multimedai\AppName\src\pages\about4\about4.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]) === "function" && _b || Object])
    ], About4Page);
    return About4Page;
    var _a, _b;
}());

//# sourceMappingURL=about4.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return About5Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the About5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var About5Page = /** @class */ (function () {
    function About5Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    About5Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad About5Page');
    };
    About5Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about5',template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\pages\about5\about5.html"*/'<!--\n  Generated template for the About1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n  \n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n      <ion-grid>\n          <ion-row>\n            <ion-col class="center">\n              <div class="c">\n                    <img src="/assets/imgs/nicknew14.svg" class="c1" >\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <div class="center">\n            <div>\n                ผู้ป่วยโรคเบาหวาน ไขมันและน้ำตาลที่ไม่ถูกย่อยสลายจะไปจับเส้นเลือด ทำให้เส้นเลือดตีบและแข็งกระทั่งเกิดการอุดตัน อาจทำให้เกิดแผลขึ้นเองโดยเฉพาะที่เท้าเนื่องจากขาดเนื้อเยื่อไปเลี้ยง  ซึ่งหากเป็นแผลก็จะหายยากเพราะหลอดเลือดตีบ ไม่มีเลือดไปหล่อเลี้ยง หรือเลือดไปหล่อเลี้ยงไม่พอ ทำให้อาจเสียขาและเสียชีวิตได้\n            </div>\n        </div>\n  \n  </ion-content>\n  '/*ion-inline-end:"D:\work2019\multimedai\AppName\src\pages\about5\about5.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], About5Page);
    return About5Page;
}());

//# sourceMappingURL=about5.js.map

/***/ }),

/***/ 114:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 114;

/***/ }),

/***/ 155:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		274,
		1
	],
	"../pages/about1/about1.module": [
		275,
		6
	],
	"../pages/about2/about2.module": [
		276,
		5
	],
	"../pages/about3/about3.module": [
		277,
		4
	],
	"../pages/about4/about4.module": [
		278,
		3
	],
	"../pages/about5/about5.module": [
		279,
		2
	],
	"../pages/about6/about6.module": [
		280,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 155;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about_about__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage.prototype.about = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__about_about__["a" /* AboutPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\pages\home\home.html"*/'<!-- <ion-header>\n  </ion-header>\n   -->\n  \n   <ion-content padding class="background-home">\n    <ion-grid>\n        <ion-row>\n          <ion-col class="center">\n            <div class="c">\n                  <img src="/assets/imgs/nick1.png" class="c1">\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n\n      <ion-grid>\n        <ion-row>\n          <ion-col class="center">\n            <div class="c">\n                  <img src="/assets/imgs/nick0.svg" class="c2" (click)="about()">\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid><br><br><br>\n  </ion-content>'/*ion-inline-end:"D:\work2019\multimedai\AppName\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(223);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_about_about__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_about1_about1__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_about2_about2__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_about3_about3__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_about4_about4__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_about5_about5__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_about6_about6__ = __webpack_require__(281);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_about1_about1__["a" /* About1Page */],
                __WEBPACK_IMPORTED_MODULE_9__pages_about2_about2__["a" /* About2Page */],
                __WEBPACK_IMPORTED_MODULE_10__pages_about3_about3__["a" /* About3Page */],
                __WEBPACK_IMPORTED_MODULE_11__pages_about4_about4__["a" /* About4Page */],
                __WEBPACK_IMPORTED_MODULE_12__pages_about5_about5__["a" /* About5Page */],
                __WEBPACK_IMPORTED_MODULE_13__pages_about6_about6__["a" /* About6Page */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/about1/about1.module#About1PageModule', name: 'About1Page', segment: 'about1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/about2/about2.module#About2PageModule', name: 'About2Page', segment: 'about2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/about3/about3.module#About3PageModule', name: 'About3Page', segment: 'about3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/about4/about4.module#About4PageModule', name: 'About4Page', segment: 'about4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/about5/about5.module#About5PageModule', name: 'About5Page', segment: 'about5', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/about6/about6.module#About6PageModule', name: 'About6Page', segment: 'about6', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_about1_about1__["a" /* About1Page */],
                __WEBPACK_IMPORTED_MODULE_9__pages_about2_about2__["a" /* About2Page */],
                __WEBPACK_IMPORTED_MODULE_10__pages_about3_about3__["a" /* About3Page */],
                __WEBPACK_IMPORTED_MODULE_11__pages_about4_about4__["a" /* About4Page */],
                __WEBPACK_IMPORTED_MODULE_12__pages_about5_about5__["a" /* About5Page */],
                __WEBPACK_IMPORTED_MODULE_13__pages_about6_about6__["a" /* About6Page */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"D:\work2019\multimedai\AppName\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]) === "function" && _c || Object])
    ], MyApp);
    return MyApp;
    var _a, _b, _c;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return About6Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the About6Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var About6Page = /** @class */ (function () {
    function About6Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    About6Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad About6Page');
    };
    About6Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about6',template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\pages\about6\about6.html"*/'<!--\n  Generated template for the About1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n  \n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n      <ion-grid>\n          <ion-row>\n            <ion-col class="center">\n              <div class="c">\n                    <img src="/assets/imgs/nicknew15.svg" class="c1" >\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <div class="center">\n            <div>\n                อ่อนเพลีย ไม่มีเเรง ไม่ว่าจะนอนพักผ่อน หรือนอนเต็มอิ่มเท่าไร ก็ยังรู้สึกว่าร่างกายพักผ่อนไม่พอ อ่อนแรง เพลีบง่าย\n            </div>\n        </div>\n  \n  </ion-content>\n  '/*ion-inline-end:"D:\work2019\multimedai\AppName\src\pages\about6\about6.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], About6Page);
    return About6Page;
}());

//# sourceMappingURL=about6.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about1_about1__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__about2_about2__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__about3_about3__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__about4_about4__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__about5_about5__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__about6_about6__ = __webpack_require__(281);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    AboutPage.prototype.home = function () {
        this.navCtrl.pop();
    };
    AboutPage.prototype.about1 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__about1_about1__["a" /* About1Page */]);
    };
    AboutPage.prototype.about2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__about2_about2__["a" /* About2Page */]);
    };
    AboutPage.prototype.about3 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__about3_about3__["a" /* About3Page */]);
    };
    AboutPage.prototype.about4 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__about4_about4__["a" /* About4Page */]);
    };
    AboutPage.prototype.about5 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__about5_about5__["a" /* About5Page */]);
    };
    AboutPage.prototype.about6 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__about6_about6__["a" /* About6Page */]);
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"D:\work2019\multimedai\AppName\src\pages\about\about.html"*/'<ion-header>\n    <img src="/assets/imgs/1111.svg" (click)="home()">\n  </ion-header>\n  \n   <ion-content padding class="background-home"><br>\n        <ion-grid>\n        <ion-row>\n          <ion-col class="center">\n            <div class="c">\n                  <img src="/assets/imgs/nicknew2.svg" class="c1" >\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <ion-col class="center">\n          <div class="button-icon" (click)="about1()">\n            <p> ปัสสาวะบ่อย </p>\n          </div>\n        </ion-col>\n      <br><br><br>\n\n      <ion-grid>\n          <ion-row>\n            <ion-col class="center">\n              <div class="c">\n                    <img src="/assets/imgs/nicknew11.svg" class="c1">\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <ion-col class="center">\n          <div class="button-icon" (click)="about2()">\n            <p> กระหายน้ำบ่อย </p>\n          </div>\n        </ion-col>\n        <br><br><br>\n    \n        <ion-grid>\n            <ion-row>\n              <ion-col class="center">\n                <div class="c">\n                      <img src="/assets/imgs/nicknick.svg" class="c1">\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n          <ion-col class="center">\n            <div class="button-icon" (click)="about3()">\n              <p> ผิวแห้งและคัน </p>\n            </div>\n          </ion-col>\n          <br><br><br>\n        \n         <ion-grid>\n              <ion-row>\n                <ion-col class="center">\n                  <div class="c">\n                        <img src="/assets/imgs/nicknew13.svg" class="c1">\n                  </div>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n            <ion-col class="center">\n              <div class="button-icon" (click)="about4()">\n                <p> น้ำหนักตัวลด </p>\n              </div>\n            </ion-col\n            ><br><br><br>\n\n            <ion-grid>\n                <ion-row>\n                  <ion-col class="center">\n                    <div class="c">\n                          <img src="/assets/imgs/nicknew14.svg" class="c1">\n                    </div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n              <ion-col class="center">\n                <div class="button-icon" (click)="about5()">\n                  <p> เป็นแผลง่าย </p>\n                </div>\n              </ion-col>\n              <br><br><br>\n            \n              <ion-grid>\n                  <ion-row>\n                    <ion-col class="center">\n                      <div class="c">\n                            <img src="/assets/imgs/nicknew15.svg" class="c1">\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n                <ion-col class="center">\n                  <div class="button-icon" (click)="about6()">\n                    <p> อ่อนเพลียง่าย </p>\n                  </div>\n                </ion-col>\n  </ion-content>'/*ion-inline-end:"D:\work2019\multimedai\AppName\src\pages\about\about.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]) === "function" && _b || Object])
    ], AboutPage);
    return AboutPage;
    var _a, _b;
}());

//# sourceMappingURL=about.js.map

/***/ })

},[200]);
//# sourceMappingURL=main.js.map